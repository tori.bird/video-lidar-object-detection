# Working in Anaconda Environment

- Download Anaconda from [here](https://www.anaconda.com/products/individual)
- Download the Python 3.7 64-Bit Graphical Installer or the 32-Bit Graphical Installer installer, per your system requirements
- Run the downloaded executable file

# Create new environment without yml file

- Open a new terminal window and paste the following command `conda create -n <env_name> pip python=3.7`
- Activate the environment with `conda activate <env_name>`
- You should now see `(<env_name>) C:\Users\user_name>`
- Install required packages for running the models

# Setting up YOLO with pytorch

- The environment yml is provided in the root of the yolo_pytorch directory
- Instantiate the environment by calling `conda env create -f environment_yolo.yml`

# Setting up Mask R-CNN with Tensorflow

- The environment yml is provided in the root of the Mask_RCNN directory
- Instantiate the environment by calling `conda env create -f environment_rcnn.yml`

# Setting up Tensorflow Object Detection API (Optional)
https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/install.html

# Setting up bulk image download

- In Git Bash window cd to base video-lidar-object-detection
- The base google-image-download package is currently broken, a fixed version has been cloned to this repo as a submodule.
- Run `cd google-images-download && sudo python setup.py install`
- If you don't already have Google Chrome on your computer download the installer from [here](https://www.google.com/chrome/) and run the exe file
- [Download the correct chrome driver for your system](https://sites.google.com/a/chromium.org/chromedriver/downloads) and place it in the data directory with the image_download notebook

# Datasets
# Bridge Dataset
The bridge dataset is tagged to work with training and testing the Mask_RCNN algorithm. The data has been split into training and validation sets along with annotation masks in the json files within those folders. The dataset was tagged with masks using [VGG Image Annotator](https://www.robots.ox.ac.uk/~vgg/software/via/via_demo.html).

# Overpass Dataset
The overpass dataset is meant to be used in training and testing the yolo object tracking algorithm. The data has been split into training and validation sets and was tagged with bounding boxes using the [labelImg](https://pypi.org/project/labelImg/1.2.7/) python package.

# Video
The video folder contains videos the algorithms can be run on.

# Running and Training Object Detection Algorithms
Each algorithm has it's own training folder or notebook and demo notebooks that can be run to train their respective algorithm. 
# Mask_RCNN
The video_detect and bridge_training should be moved into the Mask_RCNN/samples folder and should be run from within that folder. The demo notebook is very similar to the notebook already in the samples folder but changes have been made to run on a computer with one 8GB gpu. Running the bridge_training notebook will train an pretrained coco model on the bridge dataset, it will run a while. Once the training is done, the retrained model can be used in the demo or video detect notebooks. The video detect notebook will use a pretrained model to detect objects in a videos and write out to a new video.
# YOLO
A YOLO model can be trained using the train.py file in pytorch_custom_yolo_training. It's a basic YOLO training program, the number of epochs can be set in the config section. Once training is finished the trained model can be then be used to run the object detection and tracking.

# Helpful Links
# YOLO
- https://pjreddie.com/darknet/
- https://github.com/AlexeyAB/darknet#how-to-compile-on-windows-using-vcpkg
- https://towardsdatascience.com/training-yolo-for-object-detection-in-pytorch-with-your-custom-dataset-the-simple-way-1aa6f56cf7d9
- https://towardsdatascience.com/object-detection-and-tracking-in-pytorch-b3cf1a696a98

# Mask R-CNN
- https://github.com/matterport/Mask_RCNN
- https://toarches.medium.com/image-video-and-real-time-webcam-object-detection-and-instance-segmentation-with-mask-r-cnn-37a4675dcb49
- https://towardsdatascience.com/image-segmentation-using-mask-r-cnn-8067560ed773
- https://towardsdatascience.com/mask-rcnn-implementation-on-a-custom-dataset-fd9a878123d4

# Image Downloading and Annotation

- [For downloading images in bulk](https://google-images-download.readthedocs.io/en/latest/troubleshooting.html)
- [For labeling images with bounding boxes for YOLO](https://github.com/tzutalin/labelImg#labelimg)
- [For creating mask polygons for Mask R-CNN](https://www.robots.ox.ac.uk/~vgg/software/via/via_demo.html)
